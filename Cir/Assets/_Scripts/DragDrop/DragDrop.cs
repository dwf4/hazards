﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;


public class DragDrop : MonoBehaviour {

    UnityAction<GameObject> objDragger;         //Event to set the selected item
    UnityAction<bool> CheckedItems;             //Event for checking the values of placements
    private bool isCorrect = true;              //Checks if returned values from placements are correct
    private int checkCollect = 0;               //Counter to see if all values from placements are correct
    private bool collectedAll = false;          //Checks if all values have been collects and starts next step
    GameObject obj;                             //The object being dragged
    Camera cam;                                 //The Scene Camera
    Vector3 mousePos;                           //The mouse position
    public GameObject endPanel;                 //The end panel containing the text
    private TextMeshProUGUI text;               //The end text
    private GameObject[] placements;            //All placements in scene

    private void OnEnable()
    {
        EventManager.StartListening("changeObject", objDragger);
        EventManager.StartListening("ItemChecked", CheckedItems);
    }

    private void OnDisable()
    {
        EventManager.StopListening("changeObject", objDragger);
        EventManager.StopListening("ItemChecked", CheckedItems);
    }

    private void Awake()
    {
        objDragger = new UnityAction<GameObject>(NewObject);
        CheckedItems = new UnityAction<bool>(Checking);

        cam = Camera.allCameras[0];
        placements = GameObject.FindGameObjectsWithTag("Drop");

        text = endPanel.GetComponentInChildren<TextMeshProUGUI>();
        endPanel.SetActive(false);
    }

    private void Update()
    {
        if (obj)    //Code to place object in placements
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                Debug.DrawRay(ray.origin, ray.direction, Color.red, 10f);
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.transform.tag.Equals("Drop"))
                    {
                        hit.transform.GetComponent<Placements>().NewObject(obj);
                        obj = null;
                    }
                    else
                    {
                        Destroy(obj);
                    }
                }
                else
                {
                    Destroy(obj);
                }
            }
        }
    }

    private void FixedUpdate()
    {
        if (obj)    //Sets the selected object to mouse position
        {
            mousePos = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f));
            obj.transform.position = mousePos;
        }
    }

    /// <summary>
    /// A new object has been selected
    /// </summary>
    /// <param name="newObj"></param>
    private void NewObject(GameObject newObj)
    {
        if (!obj)
        {
            obj = newObj;
        } else
        {
            Destroy(newObj);
        }
    }

    /// <summary>
    /// Collects all values sent by Placements. If one is false, then it is not correct
    /// </summary>
    /// <param name="item"></param>
    private void Checking(bool item)
    {
        checkCollect++;
        if (!item)
        {
            isCorrect = false;
        }
        if (checkCollect >= placements.Length - 1)
        {
            collectedAll = true;
        }
        if (checkCollect <= 1)
        {
            StartCoroutine("waitToCheck");
        }
    }


    /// <summary>
    /// Waits until all violations have sent their value to collector
    /// </summary>
    /// <returns></returns>
    IEnumerator waitToCheck()
    {
        yield return new WaitUntil(() => collectedAll == true);
        endPanel.SetActive(true);
        if (isCorrect)
        {
            text.text = "Good Job!\nClick Anywhere to Continue";
            text.color = new Color(0, 1, 0);
        } else
        {
            text.text = "This is Incorrect!\nClick Anywhere to Restart";
            text.color = new Color(1, 0, 0);
        }
        checkCollect = 0;
    }
}
