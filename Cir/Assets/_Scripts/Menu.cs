﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Menu used in both Circut Builder and Safety Hazard projects. Some methods only apply to one of the projects.
/// </summary>
public class Menu : MonoBehaviour {

    private static Camera cam;      //The Scene Camera

    private void Start()
    {
        if (!cam)
        {
            cam = Camera.allCameras[0];
        }
    }

    /// <summary>
    /// Spawns object for Drag and Drop mechanic.
    /// </summary>
    /// <param name="obj"></param>
    public void ObjectButtons(GameObject obj)
    {
        GameObject spawn = Instantiate(obj, cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f)), Quaternion.identity); //Replace with object call
        EventManager.TriggerEvent("changeObject", spawn);
    }

    /// <summary>
    /// Enables and Disables a object
    /// </summary>
    /// <param name="obj"></param>
    public void OpenClose(GameObject obj)
    {
        obj.SetActive(!obj.activeInHierarchy);
    }

    /// <summary>
    /// Sends trigger for object for check there values
    /// </summary>
    public void SendCheck()
    {
        EventManager.TriggerEvent("CheckItems", true);
    }

    /// <summary>
    /// Resets the current scene. The scene must be in the BuildIndex for this to work.
    /// </summary>
    public void ResetScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    /// <summary>
    /// Sends Trigger to Change the Camera
    /// </summary>
    public void ChangeCamera()
    {
        EventManager.TriggerEvent("ChangeCam", true);
    }
}