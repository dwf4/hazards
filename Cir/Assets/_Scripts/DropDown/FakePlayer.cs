﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FakePlayer : MonoBehaviour
{

    public GameObject placer;               //Sprite that shows where the player is being placed
    private Renderer placerRend;            //The Renderer for the placer, used to modify its color
    public LineRenderer line;               //The line between the placer and fake first person

    private UnityAction<bool> setPlayer;    //Event to start the changing to first person controls

    private GameObject player;              //The player

    private void OnEnable()
    {
        EventManager.StartListening("firstPerson", setPlayer);
    }

    private void OnDisable()
    {
        EventManager.StopListening("firstPerson", setPlayer);
    }

    private void Awake()
    {
        setPlayer = new UnityAction<bool>(SetFirstPerson);
    }

    private void Start()
    {
        placerRend = placer.GetComponent<Renderer>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        //Update to set location and color of placer and line
        Vector3 posOffset = new Vector3(0, 0.1f, 0);
        Ray ray = new Ray(transform.position, Vector3.down);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.tag.Equals("Floor"))
            {
                placer.transform.position = hit.point + posOffset;
                placerRend.material.color = new Color(0, 1, 0);
            }else
            {
                placer.transform.position = hit.point + posOffset;
                placerRend.material.color = new Color(1, 0, 0);
            }
        } else
        {
            posOffset = new Vector3(0, -2f, 0);
            placer.transform.position = transform.position + posOffset;
            placerRend.material.color = new Color(1, 0, 0);
        }
        Vector3[] linePos = {Vector3.zero, placer.transform.localPosition};
        line.startColor = placerRend.material.color;
        line.endColor = line.startColor;
        line.SetPositions(linePos);
    }

    /// <summary>
    /// The activating and placing of the first person camera
    /// </summary>
    /// <param name="v"></param>
    void SetFirstPerson(bool v)
    {
        Vector3 posOffset = new Vector3(0, 1, 0);
        Ray ray = new Ray(transform.position, Vector3.down);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            player.transform.position = hit.point + posOffset;
            player.GetComponent<BoxCollider>().enabled = true;
        }
        Destroy(gameObject);
    }
}
