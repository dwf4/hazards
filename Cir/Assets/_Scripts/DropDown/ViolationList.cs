﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

using Mono.Data.Sqlite;
using System.Data;

public class ViolationList : MonoBehaviour
{

    private List<vioType> allViolationsList = new List<vioType>(); //All violation receieved
    private List<vioType> displayList = new List<vioType>();        //Current violations to display (search function)

    public GameObject item;
    public float itemSize;
    private RectTransform itemRect;
    public GameObject panel;
    public GameObject content;
    private RectTransform contentRect;
    public CameraControls camControls;

    private List<GameObject> listItems = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        DataFill();
        contentRect = content.GetComponent<RectTransform>();
        initialOpen();
    }

    public void initialOpen()
    {
        displayList.AddRange(allViolationsList);
        ListUpdate();
    }

    public void ListUpdate() //if you want to mess with UI elements in code, you have to work with anchor points and percentiles of the parent object
    {
        float count = displayList.Count;
        contentRect.anchorMin = new Vector2(0, 1 - (itemSize * count));
        contentRect.sizeDelta = new Vector2(0, 0);
        for (int i = 0; i < count; i++)
        {
            GameObject obj = GameObject.Instantiate(item, content.transform);
            itemRect = obj.GetComponent<RectTransform>();
            Text[] text = obj.GetComponentsInChildren<Text>();
            itemRect.pivot = new Vector2(0, 1);
            itemRect.anchorMax = new Vector2(1, 1f - (i / count));
            itemRect.anchorMin = new Vector2(0, ((count - 1) / count) - (i / count));
            itemRect.sizeDelta = new Vector2(0, 0);
            text[0].text = displayList[i].GetReference();
            text[1].text = displayList[i].GetDescription();
            obj.GetComponent<Button>().onClick.AddListener(delegate { BoxClicked(text[0].text); });
            listItems.Add(obj);
        }
    }

    private void BoxClicked(string txt)
    {
        Violation v = camControls.getCurrentViolation();
        if (v)
        {
            v.NewViolation(txt);
        }
        else
        {
            print("Error: No Violation Selected");
        }
        camControls.UnlockControls();
        panel.SetActive(false);
    }

    private void ClearList()
    {
        displayList.Clear();
        foreach (GameObject g in listItems)
        {
            Destroy(g);
        }
    }

    public void Search(TMP_InputField t)
    {
        ClearList();
        string s = t.text.ToLower();
        for (int i = 0; i < allViolationsList.Count; i++)
        {
            if (allViolationsList[i].GetDescription().ToLower().Contains(s))
            {
                displayList.Add(allViolationsList[i]);
            }
        }
        ListUpdate();
    }


    /// <summary>
    /// SQLite database to List. Don't touch if you don't know what you are doing.
    /// </summary>
    private void DataFill()
    {
        allViolationsList.Add(new vioType("None", "There is no violation."));   //Ininital add for no violation. Just in case we can't edit the database.
        string connect = "URI=file:" + Application.dataPath + "/StreamingAssets/violations.db"; //Path to database.
        IDbConnection dbConnect;
        dbConnect = (IDbConnection)new SqliteConnection(connect);
        dbConnect.Open(); //Open connection to the database.
        IDbCommand dbCommand = dbConnect.CreateCommand();
        string query = "SELECT Reference, Description FROM violation";
        dbCommand.CommandText = query;
        IDataReader reader = dbCommand.ExecuteReader();
        while (reader.Read())
        {
            string reference = reader.GetString(0);
            string description = reader.GetString(1);

            allViolationsList.Add(new vioType(reference, description));
        }
        reader.Close();
        reader = null;
        dbCommand.Dispose();
        dbCommand = null;
        dbConnect.Close();
        dbConnect = null;
    }

    public string GetDesc(string g)
    {
        string s = "No description available";
        foreach (vioType v in allViolationsList)
        {
            if (v.GetReference().ToLower().Equals(g.ToLower()))
            {
                s = v.GetDescription();
                break;
            }
        }
        return s;
    }
}
