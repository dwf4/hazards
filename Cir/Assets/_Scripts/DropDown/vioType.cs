﻿/// <summary>
/// Violation Type
/// </summary>
public class vioType
{

    private string reference;
    private string description;

    public vioType(string reference, string description)
    {
        this.reference = reference;
        this.description = description;
    }

    public string GetReference()
    {
        return reference;
    }

    public string GetDescription()
    {
        return description;
    }
}
