﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CameraControls : MonoBehaviour {

    private UnityAction<bool> checkedItems; //Event for checking items
    private bool isCorrect = true;          //Checks if returned values from violation are correct
    private int checkCollect = 0;           //Counter to see if all values from violations are correct
    private bool collectedAll = false;      //Checks if all values have been collects and starts next step
    public GameObject endPanel;             //Panel used as the parent for the text
    private TextMeshProUGUI text;           //The end text

    private UnityAction<bool> changeCam;    //Event for changing camera controls
    private Vector3 tpOffset;               //Original offset for third person camera
    private bool changingCam = false;       //Is the camera controls being changed
    public GameObject fakeFirstPerson;      //Prefab of the Fake First Person (Do Not Edit)
    private GameObject fakeFP;              //Used to display where to place first person camera
    private Vector3 mousePos;               //Gets mouse position

    private GameObject rotateObject;        //The player
    public GameObject dropDownMenu;         //The drop down menu GameObject
    private Camera cam;                     //Scene Camera

    private bool thirdPerson = true;        //Is the controls in third person
    private Vector3 input;                  //Getting player input
    private Rigidbody rb;                   //Player Rigidbody, only used in first person
    public float moveSpd;                   //Movement speed of player
    public float fpMouseSpd;                //How fast the first person rotation is

    private Violation violationChecking;    //The current violation clicked on
    private GameObject[] violations;        //All violations in the scene

    public GameObject details;
    private Text[] detailText;
    public ViolationList list;
    private bool beforeCheck = true;

    private bool isControlsLocked = false;



    private void OnEnable()
    {
        EventManager.StartListening("ChangeCam", changeCam);
        EventManager.StartListening("ItemChecked", checkedItems);
    }

    private void OnDisable()
    {
        EventManager.StopListening("ChangeCam", changeCam);
        EventManager.StopListening("ItemChecked", checkedItems);
    }

    private void Awake()
    {
        cam = Camera.allCameras[0];
        text = endPanel.GetComponentInChildren<TextMeshProUGUI>();
        dropDownMenu.SetActive(false);

        rotateObject = gameObject;
        rb = GetComponent<Rigidbody>();

        changeCam = new UnityAction<bool>(ChangeCam);
        tpOffset = cam.transform.localPosition;

        checkedItems = new UnityAction<bool>(Checking);
        violations = GameObject.FindGameObjectsWithTag("Violation");
        endPanel.SetActive(false);

        detailText = details.GetComponentsInChildren<Text>();
        details.SetActive(false);
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (!isControlsLocked)
        {
            if (!changingCam) // Is the camera beginning changed? (camera change begin)
            {
                if (thirdPerson) // Controls for if the camera is in third person
                {
                    input = (new Vector3(cam.transform.forward.x, 0, cam.transform.forward.z) * Input.GetAxisRaw("Vertical")) + (new Vector3(cam.transform.right.x, 0, cam.transform.right.z) * Input.GetAxisRaw("Horizontal"));
                    if (Input.GetKey(KeyCode.Q))
                    {
                        transform.Rotate(new Vector3(0, 1, 0), 2.5f);
                    }
                    if (Input.GetKey(KeyCode.E))
                    {
                        transform.Rotate(new Vector3(0, 1, 0), -2.5f);
                    }
                    if (Input.GetKey(KeyCode.LeftShift))
                    {
                        input += new Vector3(0, 1, 0);
                    }
                    else if (Input.GetKey(KeyCode.LeftControl))
                    {
                        input += new Vector3(0, -1, 0);
                    }
                    rb.velocity = input * moveSpd;
                }
                else // Controls for if the camera is in first person
                {
                    if (!rb.useGravity)
                    {
                        rb.useGravity = true;
                    }
                    input = (new Vector3(cam.transform.forward.x, 0, cam.transform.forward.z) * Input.GetAxisRaw("Vertical")) + (new Vector3(cam.transform.right.x, 0, cam.transform.right.z) * Input.GetAxisRaw("Horizontal"));
                    rb.velocity = Vector3.ClampMagnitude(input * moveSpd, moveSpd);
                    rb.velocity = new Vector3(rb.velocity.x, -9.8f, rb.velocity.z);
                    if (Input.GetKey(KeyCode.LeftShift))
                    {
                        changingCam = true;
                    }
                    if (Input.GetKey(KeyCode.Escape))
                    {
                        Cursor.lockState = CursorLockMode.None;
                    }
                    if (Input.GetKey(KeyCode.Tab))
                    {
                        Cursor.lockState = CursorLockMode.Locked;
                    }
                }

                // Used to find if the player clicked over a violation
                if (Input.GetKey(KeyCode.Mouse0))
                {
                    Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit))
                    {
                        if (hit.transform.tag.Equals("Violation"))
                        {
                            violationChecking = hit.transform.GetComponent<Violation>();
                            if (beforeCheck)
                            {
                                dropDownMenu.SetActive(true);
                                isControlsLocked = true;
                            }
                            else
                            {
                                details.SetActive(true);
                                detailText[0].text = "Given Violation: " + violationChecking.CheckValue();
                                detailText[1].text = "Expected Violation: " + violationChecking.expectedViolation;
                                detailText[2].text = list.GetDesc(violationChecking.expectedViolation);
                                foreach (Text t in detailText)
                                {
                                    print(t.gameObject);
                                }

                            }
                            if (!thirdPerson)
                            {
                                Cursor.lockState = CursorLockMode.None;
                            }
                        }
                    }
                }
            }
            else // (Camera Change Else)
            {
                if (thirdPerson) // Switches from third person to first person
                {
                    if (!fakeFP) //The visual effect used to show where to the player is placing the first person camera
                    {
                        fakeFP = Instantiate(fakeFirstPerson);
                    }
                    Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit))
                    {
                        mousePos = new Vector3(hit.point.x, hit.point.y + 2.5f, hit.point.z);
                        fakeFP.transform.position = mousePos;
                    }
                    if (Input.GetKey(KeyCode.Mouse0)) //Did they click over proper ground
                    {
                        ray = cam.ScreenPointToRay(Input.mousePosition);
                        if (Physics.Raycast(ray, out hit))
                        {
                            if (hit.transform.tag.Equals("Floor"))
                            {
                                //Sets the camera to first person controls
                                EventManager.TriggerEvent("firstPerson", true);
                                thirdPerson = false;
                                cam.transform.position = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);
                                Cursor.lockState = CursorLockMode.Locked;
                            }
                            else
                            {
                                Destroy(fakeFP);
                            }
                        }
                        else
                        {
                            Destroy(fakeFP);
                        }
                        changingCam = false;
                    }
                }
                else // Switches from first person to third person
                {
                    //Sets the camera to third person controls
                    thirdPerson = true;
                    rb.useGravity = false;
                    GetComponent<BoxCollider>().enabled = false;
                    cam.transform.localPosition = tpOffset;
                    cam.transform.localRotation = Quaternion.Euler(45, 0, 0);
                    Cursor.lockState = CursorLockMode.None;
                    changingCam = false;
                }
            }
        }
    }

    /// <summary>
    /// Last Update to execute, reserved for code for things such as camera rotation
    /// </summary>
    private void LateUpdate()
    {
        if (!isControlsLocked)
        {
            if (!thirdPerson) //Always Update Camera in LateUpdate or camera will feel off
            {
                if (Mathf.Abs(Input.GetAxisRaw("Mouse X")) > 0)
                {
                    float mouseX = Mathf.Clamp(Input.GetAxisRaw("Mouse X"), -2, 2);
                    transform.Rotate(new Vector3(0, 1, 0), mouseX * fpMouseSpd);
                }
                if (Mathf.Abs(Input.GetAxisRaw("Mouse Y")) > 0)
                {   // The inspector shows the rotation as -50, which translates to 310 in eulerAngles. So this round-about way to to ensure it works.
                    if ((Mathf.Sign(Input.GetAxisRaw("Mouse Y")) > 0 && (cam.transform.rotation.eulerAngles.x > 310f || cam.transform.rotation.eulerAngles.x <= 90f)) || (Mathf.Sign(Input.GetAxisRaw("Mouse Y")) < 0 && (cam.transform.rotation.eulerAngles.x >= 270f || cam.transform.rotation.eulerAngles.x < 45f)))
                    {
                        float mouseY = Mathf.Clamp(Input.GetAxisRaw("Mouse Y"), -1, 1);
                        cam.transform.Rotate(new Vector3(1, 0, 0), -mouseY * fpMouseSpd);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Recieves Signal to change camera mode
    /// </summary>
    /// <param name="v"></param>
    private void ChangeCam(bool v)
    {
        changingCam = true;
    }


    /// <summary>
    /// Collects all values sent by Violations. If one is false, then it is not correct
    /// </summary>
    /// <param name="item"></param>
    private void Checking(bool item)
    {
        checkCollect++;
        if (!item)
        {
            isCorrect = false;
        }
        if (checkCollect >= violations.Length - 1)
        {
            collectedAll = true;
        }
        if (checkCollect <= 1)
        {
            StartCoroutine("waitToCheck");
        }
    }


    /// <summary>
    /// Waits until all violations have sent their value to collector
    /// </summary>
    /// <returns></returns>
    IEnumerator waitToCheck()
    {
        yield return new WaitUntil(() => collectedAll == true);
        endPanel.SetActive(true);
        beforeCheck = false;
        if (isCorrect)
        {
            text.text = "Good Job!";
            text.color = new Color(0, 1, 0);
        }
        else
        {
            text.text = "This is Incorrect!";
            text.color = new Color(1, 0, 0);
        }
        checkCollect = 0;
    }


    public Violation getCurrentViolation()
    {
        return violationChecking;
    }

    public void UnlockControls()
    {
        isControlsLocked = false;
    }
}
