﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Placements : MonoBehaviour {

    public SpriteRenderer recievedObj; 
    public SpriteRenderer expectedObj; //Comparing a prefab and a clone will always result in false when comparing GameObjects. Comparing meshes filters or spirtes fixes that
    private UnityAction<bool> check;
    private SpriteRenderer rend;

    private void OnEnable()
    {
        EventManager.StartListening("CheckItems", check);
    }

    private void OnDisable()
    {
        EventManager.StopListening("CheckItems", check);
    }

    private void Awake()
    {
        check = new UnityAction<bool>(CheckItems);
    }


    public void NewObject(GameObject obj)
    {
        Destroy(recievedObj);
        recievedObj = obj.GetComponent<SpriteRenderer>();
        recievedObj.gameObject.transform.position = transform.position;
    }

    private void CheckItems(bool v) //Boolean v should be true by default
    {
        rend = GetComponent<SpriteRenderer>();
        if (recievedObj && expectedObj.sprite.Equals(recievedObj.sprite)) //Use sharedMesh instead of mesh for prefabs
        {
            rend.color = new Color(0, 1, 0);
        } else
        {
            rend.color = new Color(1, 0, 0);
            v = false;
        }
        EventManager.TriggerEvent("ItemChecked", v);
    }
}
