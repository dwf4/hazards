﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Event Manager
/// Singleton managing all events in scene
/// </summary>
public class EventManager : MonoBehaviour
{
    /*>> Unless you need to define new types of events, it is best to leave this as be. <<*/

    private Dictionary<string, UnityAction<GameObject>> events;
    private Dictionary<string, UnityAction<bool>> boolEvents;

    private static EventManager eventManager;

    /// <summary>
    /// Makes certain only one event manager exists
    /// </summary>
    public static EventManager instance
    {
        get
        {
            if (!eventManager)
            {
                eventManager = FindObjectOfType(typeof(EventManager)) as EventManager;
                if (!eventManager)
                {
                    print("Missing EventManager in scene.");
                }
                else
                {
                    eventManager.initalize();
                }
            }
            return eventManager;
        }
    }

    /// <summary>
    /// Sets up the dictionary of events.
    /// </summary>
    private void initalize()
    {
        if (events == null)
        {
            events = new Dictionary<string, UnityAction<GameObject>>();
        }
        if (boolEvents == null)
        {
            boolEvents = new Dictionary<string, UnityAction<bool>>();
        }
    }

    /// <summary>
    /// Instructs the event manager to listen for a specific event
    /// </summary>
    /// <param name="name"></param>
    /// <param name="action"></param>
    public static void StartListening(string name, UnityAction<GameObject> action)
    {
        if (instance != null && instance.events.ContainsKey(name))
        {
            instance.events[name] += action;
        }
        else
        {
            instance.events.Add(name, action);
        }
    }

    /// <summary>
    /// Instructs the event manager to listen for a specific event
    /// </summary>
    /// <param name="name"></param>
    /// <param name="action"></param>
    public static void StartListening(string name, UnityAction<bool> action)
    {
        if (instance != null && instance.boolEvents.ContainsKey(name))
        {
            instance.boolEvents[name] += action;
        }
        else
        {
            instance.boolEvents.Add(name, action);
        }
    }

    /// <summary>
    /// Instructs event manager to stop listening for a specific event
    /// </summary>
    /// <param name="name"></param>
    /// <param name="action"></param>
    public static void StopListening(string name, UnityAction<GameObject> action)
    {
        if (instance != null && instance.events.ContainsKey(name))
        {
            instance.events[name] -= action;
        }
    }


    /// <summary>
    /// Instructs event manager to stop listening for a specific event
    /// </summary>
    /// <param name="name"></param>
    /// <param name="action"></param>
    public static void StopListening(string name, UnityAction<bool> action)
    {
        if (instance != null && instance.boolEvents.ContainsKey(name))
        {
            instance.boolEvents[name] -= action;
        }
    }


    /// <summary>
    /// Triggers an event
    /// </summary>
    /// <param name="name"></param>
    /// <param name="value"></param>
    public static void TriggerEvent(string name, GameObject value)
    {
        UnityAction<GameObject> Event = null;
        if (instance != null && instance.events.TryGetValue(name, out Event))
        {
            Event.Invoke(value);
        }
    }

    /// <summary>
    /// Triggers an event
    /// </summary>
    /// <param name="name"></param>
    /// <param name="value"></param>
    public static void TriggerEvent(string name, bool value)
    {
        UnityAction<bool> Event = null;
        if (instance != null && instance.boolEvents.TryGetValue(name, out Event))
        {
            Event.Invoke(value);
        }
    }
}
