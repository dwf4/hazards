﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Violation : MonoBehaviour {

    public string expectedViolation;                //The violation that is expected to be entered
    private string recievedViolation = "None";      //The violation it got

    private UnityAction<bool> checking; //Event to check above values

    private Renderer rend;              //Render of GameObject

    private void OnEnable()
    {
        EventManager.StartListening("CheckItems", checking);
    }

    private void OnDisable()
    {
        EventManager.StopListening("CheckItems", checking);
    }

    private void Awake()
    {
        checking = new UnityAction<bool>(CheckItems);
        rend = GetComponent<Renderer>();
    }

    /// <summary>
    /// When the value is changed in the drop down menu
    /// </summary>
    /// <param name="i"></param>
    public void NewViolation(string i)
    {
        recievedViolation = i;
    }

    /// <summary>
    /// Gives the value so when clicked, the drop down menu has it already selected
    /// </summary>
    /// <returns></returns>
    public string CheckValue()
    {
        return recievedViolation;
    }

    /// <summary>
    /// Checks if the recieved item is the same as the expected item, changes color based on that result, and sends result to the collector via event.
    /// </summary>
    /// <param name="v"></param>
    private void CheckItems(bool v) //Boolean v should be true by default
    {
        if (expectedViolation.ToLower().Equals(recievedViolation.ToLower()))
        {
            rend.material.color = new Color(0, 1, 0, 0.2f);
        }
        else
        {
            rend.material.color = new Color(1, 0, 0, 0.2f);
            v = false;
        }
        EventManager.TriggerEvent("ItemChecked", v);
    }
}
