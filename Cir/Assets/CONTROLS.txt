FOR DRAG AND DROP:
1. Click button in menu to get object.
2. Mouse Mouse Around.
3. Click outlined sprite to place object
4. ????
5. Profit

FOR DROP DOWN:
1. Use eyes to see potential violations in red (for testing purposes)
2. If you can see any more, use Q and E to rotate around
3. Click on violation to get dropdown menu
4. Put potential violation (It will be saved and placed as the default if clicked again)
5. Close menu and repeat for rest of potential violations
6. There is no profit to be found.